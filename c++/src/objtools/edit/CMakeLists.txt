# $Id: CMakeLists.txt 648076 2022-04-07 17:00:36Z stakhovv $

NCBI_add_library(edit)
NCBI_add_subdirectory(unit_test)
NCBI_add_subdirectory(pubmed_fetch)
NCBI_add_subdirectory(test_remote_updater)
