# $Id: CMakeLists.wgs_resolver_test.app.txt 651538 2022-06-17 14:25:14Z vasilche $

NCBI_begin_app(wgs_resolver_test)
  NCBI_sources(wgs_resolver_test)
  NCBI_uses_toolkit_libraries(sraread)
  NCBI_requires(MT)

  NCBI_set_test_requires(in-house-resources)

  NCBI_project_watchers(vasilche)

NCBI_end_app()
