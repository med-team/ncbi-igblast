Source: ncbi-igblast
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: David Miguel Susano Pinto <carandraug+dev@gmail.com>,
           Steffen Moeller <moeller@debian.org>,
           Andrius Merkys <merkys@debian.org>
Build-Depends: chrpath,
               debhelper-compat (= 13),
               libmbedtls-dev,
               libncbi-vdb-dev,
               procps,
               time
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/ncbi-igblast
Vcs-Git: https://salsa.debian.org/med-team/ncbi-igblast.git
Homepage: https://ncbi.github.io/igblast/
Rules-Requires-Root: no

Package: igblast
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: ncbi-blast+
Description: Immunoglobulin and T cell receptor variable domain sequence analysis
 IgBLAST allows users to view the matches to the germline V, D, and J
 genes, details at rearrangement junctions, the delineation of IG V
 domain framework regions, and complementarity determining regions.
 IgBLAST has the capability to analyse nucleotide and protein
 sequences, and can process sequences in batches.  Furthermore,
 IgBLAST allows searches against the germline gene databases and other
 sequence databases simultaneously to minimize the chance of missing
 possibly the best matching germline V gene.
