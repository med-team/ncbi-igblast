ncbi-igblast (1.20.0-2) unstable; urgency=medium

  * Link in internal_data/ directory locally for autopkgtest, as it does not
    get automatically detected on some hosts.
  * Use chrpath to remove RUNPATH from binaries.
  * Build with system-provided mbedtls.

 -- Andrius Merkys <merkys@debian.org>  Fri, 07 Mar 2025 02:22:46 -0500

ncbi-igblast (1.20.0-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Fix lintian-overrides
  * Recommends: ncbi-blast+
  * Install edit_imgt_file.pl and c++/src/app/igblast/internal_data
  * Build-Depends: libncbi-vdb-dev, drop --without-vdb option
  * Do a more targeted build using --with-flat-makefile
  * Standards-Version: 4.6.2 (routine-update)
  * Update lintian override info format in d/source/lintian-overrides on line
    4-6.

  [ Aaron M. Ucko ]
  * debian/igblast.install: Install correct build output (Closes: #1068442)
  * debian/patches/avoid-gcc-crash.patch (new): Work around a consistent
    internal compiler error whose fix may run into the toolchain freeze:
    https://gcc.gnu.org/bugzilla/show_bug.cgi?id=108137
  * debian/rules:
    - Add possibly relevant subtrees to VDB_INCLUDE.
    - Enable optimization unless DEB_BUILD_OPTIONS contains noopt.
    - Copy-hack clean logic from ncbi-blast+.

  [ Michael R. Crusoe ]
  * build-dep on procps for the "ps" command.

  [ Andrius Merkys ]
  * Check if directory before cd'ing.
  * Subdirectory internal_data/ has to stay for igblast to find its files
    without patching.
  * Add myself to uploaders.
  * Rework debian/source/lintian-overrides to format understood by FTP server.
  * Add autopkgtest.

 -- Andrius Merkys <merkys@debian.org>  Wed, 05 Mar 2025 06:44:15 -0500

ncbi-igblast (1.19.0-1) unstable; urgency=medium

  * Team upload.
  * Fix Uploaders field
    Closes: #1016362
  * Standards-Version: 4.6.1 (routine-update)
  * Refer to common license file for Apache-2.0.

 -- Andreas Tille <tille@debian.org>  Sat, 30 Jul 2022 08:27:07 +0200

ncbi-igblast (1.18.0-1) unstable; urgency=low

  * Team upload
  * Initial Release. (Closes: #903965)

 -- Andreas Tille <tille@debian.org>  Fri, 08 Apr 2022 14:52:48 +0200
